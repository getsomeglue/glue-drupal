#!/bin/bash
#
# An example hook script to prepare the commit log message.
# Called by "git commit" with the name of the file that has the
# commit message, followed by the description of the commit
# message's source.  The hook's purpose is to edit the commit
# message file.  If the hook fails with a non-zero status,
# the commit is aborted.
#
# To enable this hook, rename this file to "prepare-commit-msg".

# This hook includes three examples. The first one removes the
# "# Please enter the commit message..." help message.
#
# The second includes the output of "git diff --name-status -r"
# into the message, just before the "git status" output.  It is
# commented because it doesn't cope with --amend or with squashed
# commits.
#
# The third example adds a Signed-off-by line to the message, that can
# still be edited.  This is rarely a good idea.

# Load variables from .env file.

export $(grep -v '^#' "$(dirname $0)/../.env" | xargs)

COMMIT_MSG_FILE=$1
COMMIT_SOURCE=$2
SHA1=$3

# Get current branch and parse the JIRA number.
CURRENT_BRANCH="$(git symbolic-ref HEAD 2>/dev/null)"
CURRENT_BRANCH=${CURRENT_BRANCH##refs/heads/}
if [[ ! -z $CURRENT_BRANCH ]]; then
  if [[ "${CURRENT_BRANCH}" =~ ^[A-Za-z]+\/([0-9]+) ]]; then
    ISSUE_NUM="${JIRA_NAME}-${BASH_REMATCH[1]}"
  else
    ISSUE_NUM="NT"
  fi

  # Get the first line of the commit message.
  read -r MSG <$COMMIT_MSG_FILE
  # If it doesn't contain the issue number, add it.
  if [[ "$MSG" != *"${ISSUE_NUM}"* ]]; then
    printf '%s: %s' "${ISSUE_NUM}" "$(cat $COMMIT_MSG_FILE)" >$COMMIT_MSG_FILE
  fi
fi
