#!/usr/bin/env bash
## Run PHPUnit tests for project.
##
## Unit and Kernel tests can be run without an existing Drupal environment. They
## can also be run _outside_ of docksal by directly calling the script (`bin/test unit,kernel`)
## as long as the local environment has sqlite and PHP installed.
##
## Example usages:
##   - fin test/phpunit unit,kernel (runs all unit and kernel tests)
##   - bin/test/phpunit unit (runs all unit tests _outside_ the container)
##   - fin test/phpunit -v src/project/a_module/tests/src/Kernel (runs all kernel tests for a given module)
# This is run within the Build/CLI container, so has all the tools.
#: exec_target = cli

source "$(dirname $0)/.test-helper"
cd ${PROJECT_ROOT}

set -e

SUDO=""
# Determine the web user.  PHPUnit must run as this user to access the files.
if [ -e ${PROJECT_ROOT}/${REL_DOCROOT}/sites/default/files ]; then
  if [ -z ${WEB_USER} ]; then
    WEB_USER=`ls -ld ${PROJECT_ROOT}/${REL_DOCROOT}/sites/default/files | awk '{print $3}'`
  fi
  SUDO="sudo -u ${WEB_USER} -E "
  case ${WEB_USER} in
    ''|*[0-9]*) SUDO="sudo -u #${WEB_USER} -E " ;;
  esac
fi
# Define default test commands.
PHPUNIT_COMMAND="${SUDO}${PROJECT_ROOT}/vendor/bin/phpunit -c ${PROJECT_ROOT}/web/tests/phpunit.xml --printer \\Drupal\\Tests\\Listeners\\HtmlOutputPrinter"

# Set DB for testing.  Using SQLite to avoid installing simpletest.
# SQLite DB must be in files folder to be shared between containers.
# For kernel tests, this directory doesn't exist, so it uses the default
# set in phpunit.xml.
SQLITE_PATH="${PROJECT_ROOT}/${REL_DOCROOT}/sites/default/files"
if [ -w  "${SQLITE_PATH}" ]; then
  export SIMPLETEST_DB="sqlite://localhost//${SQLITE_PATH}/db.sqlite"
fi

set_virtual_host

# This folder is hard-coded in core for ExistingSite tests, be sure it exists.
SIMPLETEST_OUTPUT=${PROJECT_ROOT}/${REL_DOCROOT}/sites/simpletest
if [[ ! -e $SIMPLETEST_OUTPUT ]]; then
  mkdir -p ${SIMPLETEST_OUTPUT}/browser_output
fi
chmod -R 775 ${SIMPLETEST_OUTPUT}
if [[ ! -z "${WEB_USER}" ]]; then
  chown -R ${WEB_USER} ${SIMPLETEST_OUTPUT}
fi

if [[ ! -z "${1}" && -z "${2}" ]]; then
  # Single argument is testsuite.
  printf "$(INFO_SLUG) Running ${1} tests...\n"
  ${PHPUNIT_COMMAND} --testsuite ${1}
elif [ ! -z "${2}" ]; then
  # Multiple arguments, pass to phpunit.
  printf "$(INFO_SLUG) Running tests...\n"
  ${PHPUNIT_COMMAND} ${@}
else
  # Run functional and JS functional tests. Kernel and unit tests are run in an
  # earlier step in the CI environment.
  printf "$(INFO_SLUG) Preparing to run existing-site and functional tests...\n\n"
  ${PHPUNIT_COMMAND} --testsuite existing-site,functional,functional-javascript
fi
